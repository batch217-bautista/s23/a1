// console.log("Hello World");

let PokemonTrainer = {
    name: "Ash Ketchum",
    age: 10,
    friends: {
        hoenn: ["May", "Max"],
        kanto: ["Brock", "Misty"]
    },
    pokemon: {
        pokemons: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"]
    },
    talk: function() {
        console.log("Pikachu! I choose you!");
    }
}

console.log(PokemonTrainer);
// square bracket
console.log("The trainer name is: " + PokemonTrainer['name']);

// dot notation
console.log("He repeated his name, and it's " + PokemonTrainer.name);

PokemonTrainer.talk();

let myPokemon = {
    name: "Pikachu",
    level: 3,
    health: 100,
    attack: 50,
    tackle: function(){
        console.log("This pokemon tackled target Pokemon");
        console.log("target's Pokemon's health is now reduced to _targetPokemonHealth_")
    },
    faint: function(){
        console.log("pokemon fainted");
    }
}



let myPokemon2 = {
    name: "MewTwo",
    level: 2,
    health: 65,
    attack: 25,
    tackle: function(){
        console.log("This pokemon tackled target Pokemon");
        console.log("target's Pokemon's health is now reduced to _targetPokemonHealth_")
    },
    faint: function(){
        console.log("pokemon fainted");
    }
}



let myPokemon3 = {
    name: "Geodude",
    level: 1,
    health: 10,
    attack: 5,
    tackle: function(){
        console.log("This pokemon tackled target Pokemon");
        console.log("target's Pokemon's health is now reduced to _targetPokemonHealth_")
    },
    faint: function(){
        console.log("pokemon fainted");
    }
}


function Pokemon(name, level) {
    this.name = name;
    this.level = level;
    this.health = 2 * level;
    this.attack = level; 
    let damage = this.health - this.attack;

    this.tackle = function(target){
        console.log(this.name + " tackled " + target.name);
        console.log(target.name + " is now reduced to " + damage);
        if (damage <= 0) {
            this.faint();
        }
    }
    this.faint = function(){
        console.log(this.name + " fainted");
    }
}


let pikachu = new Pokemon("Pikachu", 16);
let rattata = new Pokemon("Rattata", 8);
let mewtwo = new Pokemon("MewTwo", 4);
let geodude = new Pokemon("Geodude", 50);

console.log(pikachu);
console.log(mewtwo);
console.log(geodude);

geodude.tackle(pikachu);
console.log(pikachu);
mewtwo.tackle(geodude);